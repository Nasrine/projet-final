DROP TABLE IF EXISTS `article`; 
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `image` varchar(255) NOT NULL,
  `texte` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `auteur` varchar(255) DEFAULT NULL,
  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `titre` varchar(255) NOT NULL,
  `utilisateur` varchar(255) DEFAULT NULL,
  description varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  
  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `topic`;
CREATE TABLE `topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `titre` varchar(255) NOT NULL,
  `descripation` varchar(255) DEFAULT NULL,
  `utilisateur` varchar(255) DEFAULT NULL,
    `date` datetime DEFAULT NULL,
  
  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `nom` varchar(255) DEFAULT NULL,
  `motdepasse` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
    
  
  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

