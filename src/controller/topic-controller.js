import {Router } from "express";
import passport from "passport";
import { Comment } from "../entity/comment";
import { Commentrepository } from "../Repository/comment-repository";
import { Topicrepository } from "../Repository/topic-repository";









export const topiccontroler=Router();

topiccontroler.get('/', async (req, res)=>{
    let art=await Topicrepository.findall();
    console.log(art)
    res.json(art)
})

topiccontroler.post('/',passport.authenticate('jwt', {session:false}),async (req,res) => {
     await Topicrepository.add(req.body);
    res.status(201).json(req.body)
})

topiccontroler.put('/:id',async(req,res)=>{
    await Topicrepository.update(req.body);
    res.end()
})

topiccontroler.delete('/:id',async(req,res)=>{
    await Topicrepository.delete(req.params.id);
    res.end()
})

topiccontroler.get('/id/:id',async (req,res)=>{
    let art=await Topicrepository.findtopicByid(req.params.id)
    if (!art){
        response.status(404).json({error:'Not found'});
        return;
        
    }
    res.json(art)
})

topiccontroler.get('/month/:date', async(req, res)=>{
    let art = await Topicrepository.findByDate(req.params.date)
    res.json(art)
}
)


topiccontroler.post('/:id/comment', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        
        const topic= await Topicrepository.findtopicByid(req.params.id);

            if (!topic) {
                res.status(404).end()
                return
            }

            let comment = new Comment()
            Object.assign(comment, req.body)
            comment.user = req.user
            comment.topic = topic

            await Commentrepository.add(comment)
            res.status(201).json(comment)

        
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})