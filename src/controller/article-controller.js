import {Router } from "express";
import passport from "passport";
import { articlerepository } from "../Repository/article-repository";





export const articlecontroler=Router();

articlecontroler.get('/', async (req, res)=>{
    let art=await articlerepository.findall();
    console.log(art)
    res.json(art)
})

articlecontroler.post('/',passport.authenticate('jwt', {session:false}),async (req,res) => {
     await articlerepository.add(req.body);
    res.status(201).json(req.body)
})

articlecontroler.put('/:id',async(req,res)=>{
    await articlerepository.update(req.body);
    res.end()
})

articlecontroler.delete('/:id',async(req,res)=>{
    await articlerepository.delete(req.params.id);
    res.end()
})

articlecontroler.get('/id/:id',async (req,res)=>{
    let art=await articlerepository.findblogByid(req.params.id)
    if (!art){
        response.status(404).json({error:'Not found'});
        return;
        
    }
    res.json(art)
})

articlecontroler.get('/month/:date', async(req, res)=>{
    let art = await articlerepository.findByDate(req.params.date)
    res.json(art)
}
)
