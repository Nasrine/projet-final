import Joi from "joi";

export class User {
    nom;
    motdepasse;
    email;
    id;

constructor (nom,motdepasse,email,id ) {

    this.nom = nom;
    this.motdepasse = motdepasse;
    this.email = email;
    this.id = id;
}

toJSON() {
    return {
        id: this.id,
        email: this.email,
    }
}
}
export const userSchema = Joi.object({
    email: Joi.string().email().required(),
    motdepasse: Joi.string().min(4).required()
});
