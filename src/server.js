import express from 'express';
import cors from 'cors';
import { userController } from './controller/user-controller';
import { articlecontroler } from './controller/article-controller';
import { topiccontroler} from './controller/topic-controller';
import passport from 'passport';
import { configurePassport } from './utils/token';
import { commentcontroler } from './controller/comment-controller';

export const server = express();
configurePassport();
server.use(passport.initialize());
server.use(express.json());
server.use(cors());
server.use('/api/user',userController)
server.use('/api/article',articlecontroler)
server.use('/api/topic',topiccontroler)
server.use('/api/comment',commentcontroler)
