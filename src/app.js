import 'dotenv-flow/config';
import { server } from './server';
import { generateToken } from './utils/token';

const port = process.env.PORT || 8000;


server.listen(port, ()=> {
    console.log('listening on port '+8000);
});


// Créer un token quasi infini
// so that we can make tests
console.log(generateToken ({
    email: 'davinan@test.com',
    id: 2,
    role: 'user'
}));
