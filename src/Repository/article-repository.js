import { article } from "../entity/article";
import { connection } from "./connection";




export class articlerepository {
    //pourafficher tout les articles

    static async findall(){
        const [ rows ] = await connection.execute ( 'SELECT * FROM article');
        const articles = []
        for (let row of rows){
            let instance = new article (row.image , row.texte , row.date , row.auteur , row.id);
            articles.push(instance);
        }
        return articles ;
    }
//Ajouter un article de
static async add(article,user) {
    const [row] = await connection.query ("INSERT INTO article (image,texte,date,auteur) VALUES (?,?,?,?)", [article.image,article.texte,article.date,article.auteur]);
    return article.id = row.insertId;
        
    }
    
    //Suprimer un article

static async delete (id)
{
    const [rows] = await connection.execute ('DELETE FROM article WHERE id=?' , [id] );
} 
// un article par l'id 
static async findarticleByid (id){
    const [rows] = await connection.execute('SELECT * FROM article WHERE id=?', [id]);
    if (rows.length === 1) {
      let instance = new article(rows[0].image, rows[0].texte, rows[0].date, rows[0].auteur, rows[0].id)
        return instance;

        
    }



}

}
